import React from "react";
// import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';

const BtnMdSecondary = (props) => {
  const { namaButton, linkHref } = props;

  return (
    <button className="btn btnSecondaryMd mt-4" type="submit" href={linkHref}>
      {namaButton}
    </button>
  )
};

export default BtnMdSecondary;