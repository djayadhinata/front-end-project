import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonMediumPurple from '../components/ButtonMediumPurple';
import BtnMdSecondary from '../components/BtnMdSecondary';
import NavbarPlain from '../components/NavbarPlain';
import { ArrowLeft } from 'react-bootstrap-icons';
import inputPhotoProduk from '../assets/img/inputPhotoProduk.svg';

function InfoProdukPage() {
    return (
      <>
        {/* NAVBAR */}
        <NavbarPlain judulPage="Lengkapi Info Produk"/>

        {/* KONTEN */}
        <Container fluid className='pt-5'>

            <Row>
              <Col md={3}>
                <div className='iconArrowLeft'>
                    <ArrowLeft size={34} className='' href='/'/>
                </div>
              </Col>
            </Row>
          
            {/* Form Info Produk */}
            <Row>
              <Col lg={4}></Col>
              <Col lg={4} md={6}>
                <div className='formInfoProduk'>
                <Form className='mt-md-6' style={{paddingBottom: '22%'}} action="" method="post" enctype="">
                        <Form.Group className="mb-3 mt-4" controlId="formBasicProd">
                          <Form.Label>Nama Produk</Form.Label>
                          <Form.Control type="text" id="namaProduk" placeholder="Nama Produk" className="formRounded" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPrice">
                          <Form.Label>Harga Produk</Form.Label>
                          <Form.Control type="text" id="hargaProduk" placeholder="Rp 0.000" className="formRounded" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicCategory">
                          <Form.Label>Kategori</Form.Label>
                          <Form.Select type="text"  id="kategoriProduk" placeholder="Pilih Kategori" className="formRounded" required>
                            <option value="1">Elektronik</option>
                            <option value="2">Pakaian</option>
                            <option value="3">Alat Olahraga</option>
                            <option value="4">Alat Musik</option>
                            <option value="5">Mainan</option>
                            <option value="" selected>Semua Kategori</option>
                          </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicDesc">
                          <Form.Label>Deskripsi</Form.Label>
                          <Form.Control type="text"  id="deskripsiProduk" placeholder="Tulis deskripsi produk" className="formRounded inputLarge" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicProductPhoto">
                          {/* 
                            <Form.Label>Foto Produk<br/></Form.Label>
                            <Form.Control type="file"  id="fotoProduk" placeholder="Input foto produk" className="formRounded" required /> 
                          */}
                          <p>Foto Produk</p>
                          <img src={inputPhotoProduk} alt="foto"/>
                        </Form.Group>
                      
                        <div>
                            <BtnMdSecondary namaButton="Preview" linkHref="/"/>
                            <ButtonMediumPurple namaButton="Terbitkan" linkHref="/"/>
                        </div>
                  </Form>
                  </div>
                </Col>
                <Col md={4}></Col>
            </Row>
        </Container>
      </>
    );
  };

  export default InfoProdukPage;