import Carousel from "react-bootstrap/Carousel";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import carousel1 from "../assets/img/imgbanner.png";
import card1 from "../assets/img/card.png";
import iconSearch from "../assets/img/fi_search.png";
import NavbarHome from '../components/NavbarHome';
import "../style.css";

function HomePage() {
  return (
    <>
      {/* NAVBAR */}
      <NavbarHome/>

      {/* CAROUSEL */}
      <Container className="mt-4">
        <Carousel>
          <Carousel.Item>
            <img className="d-block w-100" src={carousel1} alt="First slide" />
            <Carousel.Caption>
              <p></p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img className="d-block w-100" src={carousel1} alt="Second slide" />

            <Carousel.Caption>
              <p></p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img className="d-block w-100" src={carousel1} alt="Third slide" />

            <Carousel.Caption>
              <p></p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </Container>

      {/* BARIS */}
      <Container>
        <br></br>
      </Container>

      {/* TELUSURI KATEGORI */}
      <Container>
        <h4>Telusuri Kategori</h4>
        <Row xs="auto">
          <Col>
            <Button className="btnPurple">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Semua
            </Button>
          </Col>
          <Col>
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Hobi
            </Button>
          </Col>
          <Col>
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Kendaraan
            </Button>
          </Col>
          <Col>
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Baju
            </Button>
          </Col>
          <Col>
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Elektronik
            </Button>
          </Col>
          <Col>
            <Button className="btnCategories">
              <img src={iconSearch} className="iconImg" alt="icon" />
              Kesehatan
            </Button>
          </Col>
        </Row>
      </Container>

      {/* BARIS */}
      <Container>
        <br></br>
      </Container>

      {/* CARD */}
      <Container>
        <Row xs={1} md={6} className="g-4">
          {Array.from({ length: 12 }).map((_, idx) => (
            <Col>
              <Card>
                <Card.Img variant="top" className="p-2" src={card1} />
                <Card.Body>
                  <Card.Title>Jam Tangan Casio</Card.Title>
                  <Card.Subtitle>Aksesoris</Card.Subtitle>
                  <Card.Text>Rp 250.000</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
      <Container>
        <Button
          className="btnPurple"
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          + Jual
        </Button>
      </Container>
    </>
  );
}

export default HomePage;
