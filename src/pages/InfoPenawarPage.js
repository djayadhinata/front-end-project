import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import ButtonLargePurp from '../components/ButtonLargePurp';
// import BtnMdSecondary from '../components/BtnMdSecondary';
import NavbarPlain from '../components/NavbarPlain';
import { ArrowLeft } from 'react-bootstrap-icons';
import buyer from '../assets/img/buyer.svg';
import jamTangan1 from '../assets/img/jamTangan1.svg';
import jamTangan11 from '../assets/img/jamTangan1.png';
import ButtonMediumPurple from '../components/ButtonMediumPurple';
import BtnMdSecondary from '../components/BtnMdSecondary';
import Modal from "react-bootstrap/Modal";

function ModalProductMatch(props) {
  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className="modalBox"
    >
      <Modal.Header closeButton></Modal.Header>
      <Modal.Body style={{marginLeft:'5%', marginRight:'5%', marginBottom:'3%'}}>
        <h6> <b>Yeay kamu berhasil mendapat harga yang sesuai</b> </h6>
        <p style={{color:"#8A8A8A"}}> Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya </p>
        {/* modal product */}
        <div 
          style={{ background: "#EEEEEE", boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)", borderRadius: "16px" }}
          classname="productMatchBox"
        >
           <div className="row" style={{ paddingTop:'4%', textAlign:'center'}}>
            <h6><b>Product Match</b></h6>
          </div>

          <div className="row" >
            <div className="col-2 p-4">
              <img src={buyer} alt="iconseller" className="fotoModal" />
            </div>
            <div className="col-10 p-3 txtModal1" style={{lineHeight: '75%', marginTop:'2%'}}>
              <h6>Nama Pembeli</h6>
              <p>Kota</p>
            </div>
          </div>

          <div className="row">
            <div className="col-2 p-4">
              <img src={jamTangan11} alt="iconseller" className="fotoModal" />
            </div>
            <div className="col-10 p-3 txtModal1" style={{lineHeight: '75%', marginTop:'2%', paddingLeft:'10%'}}>
              <h6>Jam Tangan Casio</h6>
              <p>Rp 250.000</p>
            </div>
          </div>

        </div>
        <ButtonLargePurp
          onClick={props.onHide}
          type="submit"
          namaButton="Hubungi via Whatsapp"
          hrefLink="/InfoPenawar2"
        />

      </Modal.Body>
  
    </Modal>
  );
}


function InfoPenawarPage() {
  const [modalShow, setModalShow] = React.useState(false);

    return (
      <>
       
        {/* NAVBAR */}
        <NavbarPlain judulPage="Info Penawar"/>

        {/* KONTEN */}
        <Container fluid className='pt-5'>

            <Row>
              <Col md={3}>
                <div className='iconArrowLeft'>
                    <ArrowLeft size={34} className='' href='/'/>
                </div>
              </Col>
            </Row>
          
            {/* Card Penawar */}
            <Row>
              <Col lg={12}>
                  <Container>
                    <Card className='cardPenawar'>
                        <Card.Body>
                        <div className="kontenCardPenawar">
                        <Row>
                            <Col>
                                <img src={ buyer } alt="" id="fotoBuyer"/>
                            </Col>
                            <Col>
                              <div className="txtCardPenawar">
                                <p><b>Nama Pembeli</b></p>
                                <p>Kota</p>
                              </div>
                            </Col>
                        </Row>
                        </div>
                        </Card.Body>
                    </Card>
                  </Container>
                </Col> 
            </Row>

            {/* Daftar Produk */}
            <Row>
              <Col lg={12}>
                <Container>
                  <p className="titleTawar"><b>Daftar produkmu yang ditawar</b></p>

                  <Card className="float-right cardBarangDitawar">
                    <Row>
                      <Col sm={5}>
                        <img className='imgBarangDitawar' src={ jamTangan1 } alt=""/>
                      </Col>
                      <Col sm={7}>
                        <div className="kontenCardBarangDitawar">
                            <p>27 Sept, 19:16 WIB</p>
                            <h6 className="card-title"><b>Jam Tangan Casio</b></h6>
                            <p>Rp 250.000</p>
                            <p><b>Ditawar Rp 200.000</b></p>
                            <BtnMdSecondary namaButton="Tolak" linkHref="/"/>
                            <ButtonMediumPurple namaButton="Terima" onClick={() => setModalShow(true)} linkHref="/"/>
                            <ModalProductMatch show={modalShow} onHide={() => setModalShow(false)} />
                        </div>
                     </Col>
                    </Row>
                  </Card>

                </Container>
              </Col>
            </Row>

        </Container>
      </>
    );
  };

  export default InfoPenawarPage;