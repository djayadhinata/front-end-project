import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonLargePurp from '../components/ButtonLargePurp';
import NavbarPlain from '../components/NavbarPlain';
import { ArrowLeft } from 'react-bootstrap-icons';
import inputPhotoProfile from '../assets/img/inputPhotoProfile.svg';

function InfoProfilPage() {
    return (
      <>
        {/* NAVBAR */}
        <NavbarPlain judulPage="Lengkapi Info Akun"/>

        {/* KONTEN */}
        <Container fluid className='pt-5'>

            <Row>
              <Col md={3}>
                <div className='iconArrowLeft'>
                    <ArrowLeft size={34} className='' href='/'/>
                </div>
              </Col>
            </Row>

            {/* Form Lengkapi Profil */}
            <Row>
              <Col lg={4}></Col>
              <Col lg={4} md={6}>
                <div className='formInfoProfil'>
                <Form className='mt-md-6 mb-4' style={{paddingBottom: '22%'}} action="" method="" enctype="">
                        <Form.Group className="mb-3 mt-4" controlId="formBasicProfilePhoto">
                           {/* 
                           <Form.Label>Foto Profil</Form.Label>
                            <Form.Control type="file"  id="fotoProfil" placeholder="Input foto profil" className="formRounded" required /> 
                          */}
                            <img src={inputPhotoProfile} alt="foto" style={{position:'relative', left:'38%'}}/>
                        </Form.Group>

                        <Form.Group className="mb-3 mt-4" controlId="formBasicName">
                          <Form.Label>Nama</Form.Label>
                          <Form.Control type="text" id="name" placeholder="Nama" className="formRounded" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicCity">
                          <Form.Label>Kota</Form.Label>
                          <Form.Select type="text"  id="kategoriProduk" placeholder="Pilih Kota" className="formRounded" required>
                            <option value="" selected>Pilih Kota</option>
                            <option value="1">Batam</option>
                            <option value="2">Surabaya</option>
                            <option value="3">Lombok</option>
                            <option value="4">Medan</option>
                          </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicAddress">
                          <Form.Label>Alamat</Form.Label>
                          <Form.Control type="text"  id="alamat" placeholder="Masukan alamat" className="formRounded inputLarge" required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicHP">
                          <Form.Label>Nomer Handphone</Form.Label>
                          <Form.Control type="text"  id="noHP" placeholder="Masukan no HP" className="formRounded" required />
                        </Form.Group>

                        <ButtonLargePurp namaButton="Simpan" linkHref="/"/>
                  </Form>
                  </div>
                </Col>
                <Col md={4}></Col>
            </Row>
        </Container>
      </>
    );
  };

  export default InfoProfilPage;